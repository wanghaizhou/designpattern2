package com.whz.test.decorator.finery;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class GoodPeople extends People {

    private String name;

    public GoodPeople(String name) {
        this.name = name;
    }

    public void show(){
        System.out.println("小菜【"+name+"】闪亮登场！！！");
    }

}
