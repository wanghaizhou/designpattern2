package com.whz.test.decorator.finery;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class TShirts  extends Finery{
    public TShirts(People people) {
        super(people);
    }

    void showFinery() {
        System.out.println("T恤");
    }
}
