package com.whz.test.decorator.finery;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Sneakers extends Finery {

    public Sneakers(People people) {
        super(people);
    }

    void showFinery() {
        System.out.println("运动鞋");
    }
}
