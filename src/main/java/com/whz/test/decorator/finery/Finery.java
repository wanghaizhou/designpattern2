package com.whz.test.decorator.finery;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public abstract class Finery extends People {

    private People people;

    public Finery(People people) {
        this.people = people;
    }

    public void show(){
        people.show();
        System.out.println("开始装饰");
        showFinery();
    }

    abstract void showFinery();


}
