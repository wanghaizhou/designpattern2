package com.whz.test.decorator.finery;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class People implements  ShowAble {

    /*String name;

    public People(String name) {
        this.name = name;
    }*/

    public void show(){
        System.out.println("显示");
    }

    public static void main(String[] args) {
        People people = new GoodPeople("王阿牛");
        people = new Sneakers(people);
        people = new TShirts(people);

        people.show();
    }

}
