package com.whz.test.decorator.finery;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface ShowAble {

    void show();

}
