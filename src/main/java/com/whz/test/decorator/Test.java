package com.whz.test.decorator;

import com.whz.test.decorator.template.ConcreteComponent;
import com.whz.test.decorator.template.ConcreteDecoratorA;
import com.whz.test.decorator.template.ConcreteDecoratorB;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Test {

    public static void main(String[] args) {
        ConcreteComponent concreteComponent =new ConcreteComponent();
        ConcreteDecoratorA concreteDecoratorA = new ConcreteDecoratorA(concreteComponent);
        ConcreteDecoratorB concreteDecoratorB = new ConcreteDecoratorB(concreteDecoratorA);

        concreteDecoratorB.operate();
    }

}
