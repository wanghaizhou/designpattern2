package com.whz.test.decorator.shiro;

import com.whz.test.decorator.shiro.other.EnvironmentLoaderListener;
import com.whz.test.decorator.shiro.other.FilterChainManager;
import com.whz.test.decorator.shiro.other.FilterChainResolver;
import com.whz.test.decorator.shiro.other.WebFilterChain;

/**
 * 模拟shiro 扯远了,还是回到装饰模式
 * Created by wanghaizhou on 2018/3/28.
 */
public class Test {

    public static void main(String[] args) {
        EnvironmentLoaderListener environmentLoaderListener =new EnvironmentLoaderListener();
        environmentLoaderListener.init();

        WebFilterChain webFilterChain = new WebFilterChain();
        Data data =new Data();
        data.setUsername("wanghaizhou");
        data.setPassword("123456");
        data.setRole("admin");
        data.setRequesturl("/update");
        data.setRedicturl("/www");

        FilterChainResolver filterChainResolver = environmentLoaderListener.getEnvironment().getFilterChainResolver();
        buildChains(filterChainResolver.getFilterChainManager());

        FilterChain filterChain = filterChainResolver.getChain(data,webFilterChain);
        filterChain.doFilter(data);
    }

    //本处不读取配置文件，直接写死
    private static void buildChains(FilterChainManager manager) {
        manager.addToChain("/index","login");
        manager.addToChain("/update","login");
        manager.addToChain("/update","role","admin");

    }

}
