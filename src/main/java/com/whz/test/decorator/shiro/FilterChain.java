package com.whz.test.decorator.shiro;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface FilterChain {

    void doFilter(Data data);

}
