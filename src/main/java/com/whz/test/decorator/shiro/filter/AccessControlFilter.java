package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;

import java.io.IOException;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public abstract class AccessControlFilter extends PathMatchingFilter  {

    public static final String DEFAULT_LOGIN_URL = "/login.jsp";
    private String loginUrl = "/login.jsp";

    public boolean onPreHandle(Data data, Object mappedValue) throws Exception {
        return isAccessAllowed(data, mappedValue) || onAccessDenied(data, mappedValue);
    }

    protected abstract boolean isAccessAllowed(Data var2, Object var3) throws Exception;

    protected boolean onAccessDenied(Data data, Object mappedValue) throws Exception {
        return this.onAccessDenied(data);
    }

    protected abstract boolean onAccessDenied(Data data) throws Exception;

    protected void redirectToLogin(Data data) throws IOException {
        String loginUrl = this.getLoginUrl();
        System.out.println("重定向到"+loginUrl);
    }

    protected void redirectToNoPrimission(Data data) throws IOException {
        String loginUrl = "无权限页面";
        System.out.println("重定向到"+loginUrl);
    }

    public String getLoginUrl() {
        return this.loginUrl;
    }

    public void setLoginUrl(String loginUrl) {
        this.loginUrl = loginUrl;
    }
}
