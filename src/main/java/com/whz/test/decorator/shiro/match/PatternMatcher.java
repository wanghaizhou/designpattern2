package com.whz.test.decorator.shiro.match;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface PatternMatcher {

    boolean matches(String pattern, String source);

}
