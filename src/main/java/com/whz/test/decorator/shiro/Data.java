package com.whz.test.decorator.shiro;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Data {

    /*private String username="wangdaniu";*/
    private String username;
    private String password;
    private String requesturl;
    private String redicturl;
    private boolean isAuthenticated =false;
    private String role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRedicturl() {
        return redicturl;
    }

    public void setRedicturl(String redicturl) {
        this.redicturl = redicturl;
    }

    public String getRequesturl() {
        return requesturl;
    }

    public void setRequesturl(String requesturl) {
        this.requesturl = requesturl;
    }

    public boolean isAuthenticated() {
        return isAuthenticated;
    }

    public void setAuthenticated(boolean authenticated) {
        isAuthenticated = authenticated;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "Data{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", requesturl='" + requesturl + '\'' +
                ", redicturl='" + redicturl + '\'' +
                ", isAuthenticated=" + isAuthenticated +
                ", role='" + role + '\'' +
                '}';
    }
}
