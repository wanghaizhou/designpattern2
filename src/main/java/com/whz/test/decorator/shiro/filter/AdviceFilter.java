package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.Filter;
import com.whz.test.decorator.shiro.FilterChain;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public abstract class AdviceFilter implements Filter {


    public void doFilter(Data data, FilterChain filterChain) {
        boolean continueChain = preHandle(data);

        if (continueChain) {
            executeChain(data, filterChain);
        }

        postHandle(data);

    }

    protected  void postHandle(Data data){

    }

    protected  void executeChain(Data data, FilterChain filterChain){
        filterChain.doFilter(data);
    }

    abstract  boolean preHandle(Data data);

}
