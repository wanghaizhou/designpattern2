package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.Filter;
import com.whz.test.decorator.shiro.FilterChain;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class LoginFilter extends AccessControlFilter {

    /*public void doFilter(Data data, FilterChain filterChain) {
        if (!"wanghaizhou".equals(data.getUsername())){
            System.out.println("");
        }
        return false;
    }*/

    protected boolean isAccessAllowed(Data var2, Object var3) throws Exception {
        //其实验证账号密码并不是在shior中的filter中进行的，而是再自己编写的Controller判断的
        if (!"wanghaizhou".equals(var2.getUsername())){
            System.out.println("账号不存在");
            return false;
        }
        if (!"123456".equals(var2.getPassword())){
            System.out.println("密码错误");
            return false;
        }

        System.out.println("登录通过");
        return true;

    }

    protected boolean onAccessDenied(Data data) throws Exception {
        redirectToLogin(data);
        return false;
    }
}
