package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Filter;

import java.util.LinkedHashMap;
import java.util.Map;

public enum DefaultFilter {
    /*anon(AnonymousFilter.class),
    authc(FormAuthenticationFilter.class),
    authcBasic(BasicHttpAuthenticationFilter.class),
    logout(LogoutFilter.class),
    noSessionCreation(NoSessionCreationFilter.class),
    perms(PermissionsAuthorizationFilter.class),
    port(PortFilter.class),
    rest(HttpMethodPermissionFilter.class),
    roles(RolesAuthorizationFilter.class),
    ssl(SslFilter.class),
    user(UserFilter.class);*/

    myfilter(MyFilter.class),
    role(RoleFilter.class),
    login(LoginFilter.class);

    private final Class<? extends Filter> filterClass;

    private DefaultFilter(Class<? extends Filter> filterClass) {
        this.filterClass = filterClass;
    }

    public Filter newInstance() {
        try {
            return (Filter)this.filterClass.newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Class<? extends Filter> getFilterClass() {
        return this.filterClass;
    }

    public static Map<String, Filter> createInstanceMap() {
        Map<String, Filter> filters = new LinkedHashMap(values().length);
        DefaultFilter[] arr$ = values();
        int len$ = arr$.length;

        for(int i$ = 0; i$ < len$; ++i$) {
            DefaultFilter defaultFilter = arr$[i$];
            Filter filter = defaultFilter.newInstance();
            filters.put(defaultFilter.name(), filter);
        }

        return filters;
    }
}