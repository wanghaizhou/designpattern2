package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class RoleFilter extends AccessControlFilter{

    //写死判断,省略shiro的Subject的权限判断
    protected boolean isAccessAllowed(Data var2, Object var3) throws Exception {
        /*String needrole = (String) appliedPaths.get(var2.getRequesturl());*/
        if (var3!=null){
            String[] params = (String[]) var3;
            /*System.out.println("需要的权限："+param);*/
            for (String param:params){
                if (var2!=null&&var2.getRole().equals(param)){
                    System.out.println(param+"权限通过");
                    return true;
                }
            }

        }

        return false;
    }

    protected boolean onAccessDenied(Data data) throws Exception {
        if (data.isAuthenticated()){
            redirectToNoPrimission(data);
        }
        redirectToLogin(data);
        return false;
    }
}
