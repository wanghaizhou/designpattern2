package com.whz.test.decorator.shiro.other;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.FilterChain;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class WebFilterChain implements FilterChain {

    public void doFilter(Data data) {
        System.out.println("模拟web的FilterChain执行完毕:"+data);
    }

}
