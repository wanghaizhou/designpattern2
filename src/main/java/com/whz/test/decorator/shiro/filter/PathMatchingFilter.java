package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.Filter;
import com.whz.test.decorator.shiro.match.AntPathMatcher;
import com.whz.test.decorator.shiro.match.PatternMatcher;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public abstract class PathMatchingFilter extends AdviceFilter implements PathConfigProcessor {

    PatternMatcher antPathMatcher = new AntPathMatcher();

    //该过滤器需要过滤的地址及相关处理参数   如 shiro权限配置 /role=authc,roles[admin]  对应值为  role：admin
    protected Map<String, Object> appliedPaths = new LinkedHashMap<String, Object>();

    //有无权限继续执行处理器
    boolean preHandle(Data data) {
        if (this.appliedPaths == null || this.appliedPaths.isEmpty()) {
            System.out.println("参数空掉了");
            return true;
        }

        for (String path : this.appliedPaths.keySet()) {
            if (pathsMatch(path, data.getRequesturl())) {
                System.out.println("功能匹配当前请求地址"+path+";执行过滤链");
                Object config = this.appliedPaths.get(path);//参数
                try {
                    return isFilterChainContinued(data, path, config);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return true;
    }

    private boolean pathsMatch(String path, String requesturl) {
        return antPathMatcher.matches(path,requesturl);
    }

    protected boolean onPreHandle(Data data, Object mappedValue) throws Exception {
        return true;
    }

    private boolean isFilterChainContinued(Data data,
                                           String path, Object pathConfig) throws Exception {
        return onPreHandle(data, pathConfig);
    }

    //往过滤器添加需要过滤的地址及相关参数config(比如权限的字符串形式)
    public Filter processPathConfig(String path, String config) {
        String[] values = null;
        if (config != null) {
            //values = split(config);
            values = config.split(",");
        }

        this.appliedPaths.put(path, values);
        return this;
    }
}
