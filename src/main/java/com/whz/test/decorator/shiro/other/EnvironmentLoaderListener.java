package com.whz.test.decorator.shiro.other;

/**
 * 长话短说
 * Created by wanghaizhou on 2018/3/28.
 */
public class EnvironmentLoaderListener {

    private  WebEnvironment environment;

    public void init(){
        environment = createEnvironment();
    }

    protected WebEnvironment createEnvironment(/*ServletContext sc*/) {
        Class clazz = null;
        try {
            clazz = Thread.currentThread().getContextClassLoader().loadClass("com.whz.test.decorator.shiro.other.IniWebEnvironment");
            WebEnvironment environment = (WebEnvironment) clazz.newInstance();
            return environment;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public  WebEnvironment getEnvironment() {
        return environment;
    }

    public  void setEnvironment(WebEnvironment environment) {
        this.environment = environment;
    }
}
