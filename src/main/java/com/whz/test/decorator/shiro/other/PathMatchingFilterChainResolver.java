package com.whz.test.decorator.shiro.other;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.FilterChain;
import com.whz.test.decorator.shiro.ProxiedFilterChain;
import com.whz.test.decorator.shiro.match.AntPathMatcher;
import com.whz.test.decorator.shiro.match.PatternMatcher;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class PathMatchingFilterChainResolver implements FilterChainResolver {

    private FilterChainManager filterChainManager;

    private PatternMatcher pathMatcher;

    public PathMatchingFilterChainResolver() {
        this.pathMatcher = new AntPathMatcher();
        this.filterChainManager = new DefaultFilterChainManager();
    }


    public FilterChain getChain(Data data, FilterChain originalChain) {
        FilterChainManager filterChainManager = getFilterChainManager();
        if (!filterChainManager.hasChains()) {
            return null;
        }

        String requestURI = data.getRequesturl();

        //the 'chain names' in this implementation are actually path patterns defined by the user.  We just use them
        //as the chain name for the FilterChainManager's requirements
        for (String pathPattern : filterChainManager.getChainNames()) {
            // If the path does match, then pass on to the subclass implementation for specific checks:
            if (pathMatches(pathPattern, requestURI)) {
                return new ProxiedFilterChain(originalChain, filterChainManager.getChain(pathPattern));
            }
        }

        return originalChain;
    }

    public FilterChainManager getFilterChainManager() {
        return filterChainManager;
    }

    public void setFilterChainManager(FilterChainManager filterChainManager) {
        this.filterChainManager = filterChainManager;
    }

    public PatternMatcher getPathMatcher() {
        return pathMatcher;
    }

    public void setPathMatcher(PatternMatcher pathMatcher) {
        this.pathMatcher = pathMatcher;
    }

    protected boolean pathMatches(String pattern, String path) {
        PatternMatcher pathMatcher = getPathMatcher();
        return pathMatcher.matches(pattern, path);
    }
}
