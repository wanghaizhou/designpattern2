package com.whz.test.decorator.shiro.other;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.FilterChain;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface FilterChainResolver {

    FilterChain getChain(Data data, FilterChain originalChain);

    FilterChainManager getFilterChainManager();

    /*FilterChain getChain(ServletRequest request, ServletResponse response, FilterChain originalChain);*/

}
