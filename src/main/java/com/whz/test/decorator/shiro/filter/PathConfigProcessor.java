package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Filter;

public interface PathConfigProcessor {

    Filter processPathConfig(String path, String config);

}