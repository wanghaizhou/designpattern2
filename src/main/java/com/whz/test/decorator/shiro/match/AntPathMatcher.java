package com.whz.test.decorator.shiro.match;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class AntPathMatcher implements PatternMatcher {

    //省略复杂的web请求地址匹配
    public boolean matches(String pattern, String source) {
        if (pattern!=null&&pattern.equals(source)){
            return true;
        }
        return false;
    }
}
