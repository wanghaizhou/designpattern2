package com.whz.test.decorator.shiro.other;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface WebEnvironment {

    FilterChainResolver getFilterChainResolver();

    WebSecurityManager getWebSecurityManager();

    //长话短说
    /*ServletContext getServletContext();*/

}
