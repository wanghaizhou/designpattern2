package com.whz.test.decorator.shiro.other;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class IniWebEnvironment implements WebEnvironment {

    FilterChainResolver resolver;

    public IniWebEnvironment() {
        resolver = new PathMatchingFilterChainResolver();
        FilterChainManager manager = resolver.getFilterChainManager();
        /*buildChains(manager, ini);*/
        /*buildChains(manager);*/
        /*WebIniSecurityManagerFactory factory = new WebIniSecurityManagerFactory();*/
    }

    //本处不读取配置文件，直接写死
    /*private void buildChains(FilterChainManager manager) {
        manager.addToChain("/index","login");
        manager.addToChain("/update","myfilter","admin");
        manager.addToChain("/update","login");
    }*/

    public FilterChainResolver getFilterChainResolver() {
        return resolver;
    }

    //篇幅原因，直接废掉WebSecurityManager
    public WebSecurityManager getWebSecurityManager() {
        return null;
    }
}
