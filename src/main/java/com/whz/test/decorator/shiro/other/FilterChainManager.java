package com.whz.test.decorator.shiro.other;

import com.whz.test.decorator.shiro.Filter;

import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface FilterChainManager {

    void addToChain(String chainName, String filterName);

    void addToChain(String chainName, String filterName, String chainSpecificFilterConfig);

    Map<String, Filter> getFilters();

    void addFilter(String name, Filter filter);

    List<Filter> getChain(String chainName);

    boolean hasChains();

    Set<String> getChainNames();
}
