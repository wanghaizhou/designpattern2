package com.whz.test.decorator.shiro;

import java.util.List;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ProxiedFilterChain implements FilterChain {

    private FilterChain filterChain;

    private List<Filter> filters;

    private Integer index =0;

    public ProxiedFilterChain(FilterChain filterChain, List<Filter> filters) {
        this.filterChain = filterChain;
        this.filters = filters;
    }

    public void doFilter(Data data) {
        if (filters==null||index==filters.size()){
            filterChain.doFilter(data);
        }else {
            /*filters.get(index++).doFilter(data,filterChain); //错误方式*/
            filters.get(index++).doFilter(data,this);
        }
    }
}
