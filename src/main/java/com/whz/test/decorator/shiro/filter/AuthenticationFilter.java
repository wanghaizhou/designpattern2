package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public abstract class AuthenticationFilter extends AccessControlFilter {
    protected boolean isAccessAllowed(Data data, Object mappedValue) {
        /*return super.isAccessAllowed(data, mappedValue);*/
        if (data.isAuthenticated()){
            return true;
        }
        return false;
    }
}
