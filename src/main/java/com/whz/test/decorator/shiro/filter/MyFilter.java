package com.whz.test.decorator.shiro.filter;

import com.whz.test.decorator.shiro.Data;
import com.whz.test.decorator.shiro.Filter;
import com.whz.test.decorator.shiro.FilterChain;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class MyFilter extends AdviceFilter {

    boolean preHandle(Data data) {
        if (!"wanghaizhou".equals(data.getUsername())){
            System.out.println("");
        }
        return false;
    }
}
