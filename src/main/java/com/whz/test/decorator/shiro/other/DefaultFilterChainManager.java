package com.whz.test.decorator.shiro.other;

import com.whz.test.decorator.shiro.Filter;
import com.whz.test.decorator.shiro.filter.DefaultFilter;
import com.whz.test.decorator.shiro.filter.PathConfigProcessor;

import java.util.*;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class DefaultFilterChainManager implements FilterChainManager {

    private Map<String, Filter> filters; //pool of filters available for creating chains

    private Map<String, List<Filter>> filterChains; //key: chain name, value: chain

    public DefaultFilterChainManager() {
        this.filters = new LinkedHashMap<String, Filter>();
        this.filterChains = new LinkedHashMap<String, List<Filter>>();
        addDefaultFilters(false);
    }

    protected void addDefaultFilters(boolean init) {
        for (DefaultFilter defaultFilter : DefaultFilter.values()) {
            addFilter(defaultFilter.name(), defaultFilter.newInstance());
        }
    }

    public void addToChain(String chainName, String filterName) {
        addToChain(chainName,filterName,null);
    }

    public void addToChain(String chainName, String filterName, String chainSpecificFilterConfig) {
        Filter filter = getFilter(filterName);
        if (filter == null) {
            throw new IllegalArgumentException("There is no filter with name '" + filterName +
                    "' to apply to chain [" + chainName + "] in the pool of available Filters.  Ensure a " +
                    "filter with that name/path has first been registered with the addFilter method(s).");
        }

        if (filter instanceof PathConfigProcessor) {
            ((PathConfigProcessor) filter).processPathConfig(chainName, chainSpecificFilterConfig);
        }else {

        }
        if (!filterChains.containsKey(chainName)){
            filterChains.put(chainName,new ArrayList<Filter>());
        }
        filterChains.get(chainName).add(filter);
    }

    public Map<String, Filter> getFilters() {
        return filters;
    }

    public void addFilter(String name, Filter filter) {
        if (!filters.containsKey(name)){
            filters.put(name,filter);
        }
    }

    public List<Filter> getChain(String chainName) {
        return filterChains.get(chainName);
    }

    public boolean hasChains() {
        return this.filterChains!=null&&this.filterChains.size()>0;
    }

    public Filter getFilter(String name) {
        return this.filters.get(name);
    }

    public Set<String> getChainNames() {
        //noinspection unchecked
        return this.filterChains != null ? this.filterChains.keySet() : Collections.EMPTY_SET;
    }
}
