package com.whz.test.decorator.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface Component {

    void operate();

}
