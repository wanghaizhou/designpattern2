package com.whz.test.decorator.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ConcreteDecoratorB extends  Decorator{


    public ConcreteDecoratorB(Component component) {
        super(component);
    }

    public void operate() {
        super.operate();
        System.out.println("B装饰一下");
    }

}
