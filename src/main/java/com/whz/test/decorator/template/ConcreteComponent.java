package com.whz.test.decorator.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ConcreteComponent implements Component {

    public void operate() {
        System.out.println("真正执行的操作");
    }

}
