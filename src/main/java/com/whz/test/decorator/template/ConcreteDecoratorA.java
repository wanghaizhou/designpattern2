package com.whz.test.decorator.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ConcreteDecoratorA extends  Decorator{


    public ConcreteDecoratorA(Component component) {
        super(component);
    }

    public void operate() {
        super.operate();
        System.out.println("A装饰一下");
    }

}
