package com.whz.test.decorator.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Decorator implements Component{

    private Component component;

    public Decorator(Component component) {
        this.component = component;
    }

    public void operate() {
        component.operate();
        System.out.println("开始装饰");
    }
}
