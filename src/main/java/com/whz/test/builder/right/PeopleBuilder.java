package com.whz.test.builder.right;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface PeopleBuilder {

    void buildRightArm();
    void buildleftArm();

    void buildRightLeg();
    void buildLeftLeg();

    void buildBody();

    void buildHead();

}
