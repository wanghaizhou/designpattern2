package com.whz.test.builder.right;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class PeopleDirector {

    PeopleBuilder peopleBuilder;

    public PeopleDirector(PeopleBuilder peopleBuilder) {
        this.peopleBuilder = peopleBuilder;
    }

    public void buildPeople(){
        peopleBuilder.buildHead();
        peopleBuilder.buildBody();
        peopleBuilder.buildRightArm();
        peopleBuilder.buildleftArm();
        peopleBuilder.buildRightLeg();
        peopleBuilder.buildLeftLeg();
    }
}
