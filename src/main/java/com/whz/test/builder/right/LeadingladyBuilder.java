package com.whz.test.builder.right;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class LeadingladyBuilder implements PeopleBuilder {

    private String name;

    public LeadingladyBuilder(String name) {
        this.name = name;
    }

    public void buildRightArm() {
        System.out.println(getName()+"的细手1");
    }

    public void buildleftArm() {
        System.out.println(getName()+"的细手2");
    }

    public void buildRightLeg() {
        System.out.println(getName()+"的细长腿1");
    }

    public void buildLeftLeg() {
        System.out.println(getName()+"的细长腿2");
    }

    public void buildBody() {
        System.out.println(getName()+"的健美躯干");
    }

    public void buildHead() {
        System.out.println(getName()+"的漂亮脸蛋");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
