package com.whz.test.builder.right;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class LurenABuilder implements PeopleBuilder {
    private String name;

    public LurenABuilder(String name) {
        this.name = name;
    }

    public void buildRightArm() {
        System.out.println(getName()+"的手1");
    }

    public void buildleftArm() {
        System.out.println(getName()+"的手2");
    }

    public void buildRightLeg() {
        System.out.println(getName()+"的腿1");
    }

    public void buildLeftLeg() {
        System.out.println(getName()+"的腿2");
    }

    public void buildBody() {
        System.out.println(getName()+"的躯干");
    }

    public void buildHead() {
        System.out.println(getName()+"的大头");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
