package com.whz.test.builder.right;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Test {

    public static void main(String[] args) {
        System.out.println("构造者模式");
        PeopleDirector peopleDirector = new PeopleDirector(new LeadingladyBuilder("小美"));
        PeopleDirector peopleDirector2 = new PeopleDirector(new LurenABuilder("路人甲"));
        peopleDirector.buildPeople();
        System.out.println("+++++++++++++++++++++++++++++++++++++++++");
        peopleDirector2.buildPeople();
    }

}
