package com.whz.test.builder.mytest;

/**
 * 平时习惯写的代码（直接了当,不适合维护修改）
 * 回忆：什么是依赖倒置原则，抽象不应该依赖于细节，细节应该依赖于抽象
 * Created by wanghaizhou on 2018/3/28.
 */
public class People {

    public static void main(String[] args) {
        new People().draw();
    }


    //画出来
    public void draw(){
        System.out.println("细手1");
        System.out.println("细手2");
        System.out.println("细长腿1");
        System.out.println("细长腿2");
        System.out.println("瘦肚子");
        System.out.println("头");
        System.out.println("女主角");
    }


    /*public void draw(){
        System.out.println(" |  口  ");
        System.out.println(" ===|===");
        System.out.println("    口  |");
        System.out.println("  |   |");
        System.out.println("  |   |");
        System.out.println(" _|   |_");
    }*/

}
