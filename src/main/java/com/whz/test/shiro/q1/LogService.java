package com.whz.test.shiro.q1;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface LogService {

    void logbefore();

    void logafter();

    void logError();

}
