package com.whz.test.shiro.q1;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class LogAndCountServiceImpl implements LogService {

    /*private int count=1;*/

    private LogProxy logProxy;

    public LogAndCountServiceImpl(LogProxy logProxy) {
        this.logProxy = logProxy;
    }

    public void logbefore() {
        System.out.println("第"+logProxy.getAndAdd()+"次执行 Q1test 操作前的一堆处理");
    }

    public void logafter() {
        System.out.println("Q1test 操作后一堆处理");
    }

    public void logError() {
        System.out.println("Q1test 操作异常一堆处理");
    }

    /*public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }*/
}
