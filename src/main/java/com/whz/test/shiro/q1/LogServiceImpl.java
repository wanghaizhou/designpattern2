package com.whz.test.shiro.q1;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class LogServiceImpl implements LogService {

    public void logbefore() {
        System.out.println("Q1test 操作前的一堆处理");
    }

    public void logafter() {
        System.out.println("Q1test 操作后一堆处理");
    }

    public void logError() {
        System.out.println("Q1test 操作异常一堆处理");
    }
}
