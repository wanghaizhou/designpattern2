package com.whz.test.shiro.q1;

import java.util.concurrent.Callable;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class LogProxy {

    /*LogService logService =new LogServiceImpl();*/
    LogService logService =new LogAndCountServiceImpl(this);
    private int count=0;

    public Object execute(Callable callable){
        try {
            logService.logbefore();
            return callable.call();
        } catch (Exception e) {
            e.printStackTrace();
            logService.logError();
        }finally {
            logService.logafter();
        }
        return null;
    }

    public Integer getAndAdd(){
        count++;
        return count;
    }

}
