package com.whz.test.shiro.q1;

import java.util.concurrent.Callable;

/**
 * 需要达到test2方法的效果  代理模式？
 * Created by wanghaizhou on 2018/3/28.
 */
public class Q1test {

    public static void main(String[] args) {
        LogProxy logProxy = new LogProxy();
        final Q1test q1test = new Q1test();
        for (int i=0;i<10;i++){
            String info = (String) logProxy.execute(new Callable() {
                public Object call() throws Exception {
                    String info = q1test.do1();
                    System.out.println(info);
                    return info;
                }
            });
            /*System.out.println(info);*/
        }

    }

    private String do1(){
        return "操作结果已经出来";
    }


    // '一堆处理' 代表处理逻辑复杂，同一个方法将包含多行处理逻辑，故需要要讲这些操作移动到某个特定的处理类(LogService)中处理
    private void test2(){
        final Q1test q1test = new Q1test();
        System.out.println("Q1test 操作前的一堆处理");
        try {
            String name = q1test.do1();
        } catch (Exception e) {
            System.out.println("Q1test 操作异常一堆处理");
        }finally {
            System.out.println("Q1test 操作后一堆处理");
        }
    }

}
