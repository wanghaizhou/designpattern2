package com.whz.test.facade.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class Realty {

    public void buy(){
        System.out.println("购买房地产基金1");
    }

    public void sell(){
        System.out.println("抛售地产基金1");
    }

}
