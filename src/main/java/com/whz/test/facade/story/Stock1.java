package com.whz.test.facade.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class Stock1 {
    public void buy(){
        System.out.println("购买股票1");
    }

    public void sell(){
        System.out.println("抛售股票1");
    }
}
