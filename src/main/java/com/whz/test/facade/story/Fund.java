package com.whz.test.facade.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class Fund {

    Stock1 stock1 =new Stock1();
    Stock2 stock2 =new Stock2();
    Realty realty =new Realty();

    public static void main(String[] args) {
        Fund fund =new Fund();
        fund.buy();
        fund.sell();
    }

    public void buy(){
        stock1.buy();
        stock2.buy();
        realty.buy();
    }

    public void sell(){
        stock1.sell();
        stock2.sell();
        realty.sell();
    }

}
