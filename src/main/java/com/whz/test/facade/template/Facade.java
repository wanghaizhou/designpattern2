package com.whz.test.facade.template;

/**
 * Created by 海州 on 2018/4/1.
 */
public class Facade {

    SubSystem1 subSystem1 = new SubSystem1();
    SubSystem2 subSystem2 = new SubSystem2();

    public void fuction(){
        subSystem1.method1();
        subSystem2.method1();
    }

    public static void main(String[] args) {
        new Facade().fuction();
    }

}
