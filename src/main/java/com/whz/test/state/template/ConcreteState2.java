package com.whz.test.state.template;

/**
 * Created by 海州 on 2018/4/1.
 */
public class ConcreteState2 implements State{

    @Override
    public void handle(Context context) {
        System.out.println("处理2之后状态变成1");
        context.setState(new ConcreteState1());
    }
}
