package com.whz.test.state.template;

/**
 * Created by 海州 on 2018/4/1.
 */
public class Context {

    private State state;

    public Context(State state) {
        this.state = state;
    }

    public void request(){
        state.handle(this);
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }
}
