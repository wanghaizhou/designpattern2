package com.whz.test.state.template;

/**
 * Created by 海州 on 2018/4/1.
 */
public interface State {

    void handle(Context context);

}
