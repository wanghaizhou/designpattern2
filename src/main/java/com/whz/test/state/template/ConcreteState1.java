package com.whz.test.state.template;

/**
 * Created by 海州 on 2018/4/1.
 */
public class ConcreteState1 implements State{

    @Override
    public void handle(Context context) {
        System.out.println("处理1之后状态变成2");
        context.setState(new ConcreteState2());
    }

}
