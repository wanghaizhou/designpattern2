package com.whz.test.state.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class NoonState implements State{
    @Override
    public void writeProgram(Work work) {
        if (work.getCurrentHour()<14){
            System.out.println("中午午休时间;现在时间"+work.getCurrentHour());
        }else {
            work.setState(new AfterNoonState());
            work.writeProgram();
        }
    }
}
