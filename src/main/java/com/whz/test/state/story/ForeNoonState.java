package com.whz.test.state.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class ForeNoonState implements State{
    @Override
    public void writeProgram(Work work) {
        if (work.getCurrentHour()<=12){
            System.out.println("早上精力旺盛;现在时间"+work.getCurrentHour());
        }else {
            work.setState(new NoonState());
            work.writeProgram();
        }
    }
}
