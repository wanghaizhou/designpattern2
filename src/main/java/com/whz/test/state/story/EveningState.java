package com.whz.test.state.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class EveningState implements State {
    @Override
    public void writeProgram(Work work) {
        if (work.isFinished()){
            work.setState(new RestState());
            work.writeProgram();
        }else {
            if (work.getCurrentHour()<21){
                System.out.println("晚上加班；现在时间"+work.getCurrentHour());
            }else {
                work.setState(new SleepState());
                work.writeProgram();
            }
        }
    }
}
