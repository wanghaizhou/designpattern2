package com.whz.test.state.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class Work {

    boolean isFinished=false;

    int currentHour;

    private State state;

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public int getCurrentHour() {
        return currentHour;
    }

    public void setCurrentHour(int currentHour) {
        this.currentHour = currentHour;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public void writeProgram(){
        state.writeProgram(this);
    }

    public static void main(String[] args) {
        Work work =new Work();
        work.setState(new ForeNoonState());
        for (int i=7;i<=21;i++){
            if (i==14){
                System.out.println();
            }
            if (i==20){
                work.setFinished(true);
            }
            work.setCurrentHour(i);
            work.writeProgram();
        }
    }
}
