package com.whz.test.state.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public interface State {

    void writeProgram(Work work);

}
