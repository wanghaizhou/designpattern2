package com.whz.test.state.story;

/**
 * Created by 海州 on 2018/4/1.
 */
public class AfterNoonState implements State{
    @Override
    public void writeProgram(Work work) {
        if (work.getCurrentHour()<=17){
            System.out.println("下午状态，继续努力;现在时间"+work.getCurrentHour());
        }else {
            work.setState(new EveningState());
            work.writeProgram();
        }
    }
}
