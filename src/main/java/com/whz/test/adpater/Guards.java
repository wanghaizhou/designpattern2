package com.whz.test.adpater;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Guards implements Player {

    String name;

    public Guards(String name) {
        this.name = name;
    }

    public void attack() {
        System.out.println("后卫"+name+"进攻");
    }

    public void defence() {
        System.out.println("后卫"+name+"防守");
    }

}
