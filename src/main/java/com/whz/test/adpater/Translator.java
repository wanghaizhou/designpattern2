package com.whz.test.adpater;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Translator implements Player {

    /*ForeignPlayer foreignPlayer = new ForeignCenter("姚明");*/
    ForeignPlayer foreignPlayer;

    public Translator(ForeignPlayer foreignPlayer) {
        this.foreignPlayer = foreignPlayer;
    }

    public void attack() {
        foreignPlayer.进攻();
    }

    public void defence() {
        foreignPlayer.防守();
    }
}
