package com.whz.test.adpater;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface Player {

    void attack();

    void defence();

}
