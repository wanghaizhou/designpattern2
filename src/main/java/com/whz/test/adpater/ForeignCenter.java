package com.whz.test.adpater;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ForeignCenter implements ForeignPlayer{

    String name;

    public ForeignCenter(String name) {
        this.name = name;
    }

    public void 进攻() {
        System.out.println("后卫"+name+"进攻");
    }

    public void 防守() {
        System.out.println("后卫"+name+"防守");
    }

}
