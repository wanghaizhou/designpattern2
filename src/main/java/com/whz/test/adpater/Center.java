package com.whz.test.adpater;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Center implements Player {

    String name;

    public Center(String name) {
        this.name = name;
    }

    public void attack() {
        System.out.println("中锋"+name+"进攻");
    }

    public void defence() {
        System.out.println("中锋"+name+"防守");
    }
}
