package com.whz.test.adpater;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Test {

    public static void main(String[] args) {
        ForeignPlayer foreignPlayer = new ForeignCenter("姚明");
        Translator translator = new Translator(foreignPlayer);
        translator.attack();
        translator.defence();
    }

}
