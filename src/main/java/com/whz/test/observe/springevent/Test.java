package com.whz.test.observe.springevent;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"com.whz.test.observe.springevent"})
public class Test {
    public static void main(String[] args) {
        AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(Test.class);
        DemoPublisher publisher = context.getBean(DemoPublisher.class);
        // 发布事件
        publisher.published();
        context.close();
    }
}
