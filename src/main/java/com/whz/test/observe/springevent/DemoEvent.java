package com.whz.test.observe.springevent;

import lombok.Data;
import org.springframework.context.ApplicationEvent;

@Data
// 监听器监听的对象
public class DemoEvent extends ApplicationEvent {
 
    private String msg;
    public DemoEvent(Object source,String msg) {
        super(source);
        this.setMsg(msg);
    }
}
