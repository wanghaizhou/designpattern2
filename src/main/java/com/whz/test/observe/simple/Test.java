package com.whz.test.observe.simple;

import java.util.Observer;

public class Test {
    public static void main(String[] args) {
		// create watcher
		Watched watched = new Watched();
		// create observer
		Observer watcher = new Watcher(watched);
		// set data
		watched.setData("now");
	}
}
