package com.whz.test.observe.simple;

import java.util.Observable;

// 被监听对象
public class Watched extends Observable {
    private String data = "";
    
    public String getData() {
        return data;
    }
 
    public void setData(String data) {
        
        if(!this.data.equals(data)){
            this.data = data;
            // 修改状态
            setChanged();
        }
        // 通知所有观察者
        notifyObservers();
    }
}
