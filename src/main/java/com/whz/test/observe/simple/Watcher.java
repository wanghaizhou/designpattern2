package com.whz.test.observe.simple;

import java.util.Observable;
import java.util.Observer;

// 监听器
public class Watcher implements Observer {
 
	public Watcher(Observable o) {
        o.addObserver(this);
	}
 
	@Override
	public void update(Observable o, Object arg) {
		System.out.println("statue changed：" + ((Watched) o).getData());
	}
}
