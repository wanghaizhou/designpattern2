package com.whz.test.proxy.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface Subject {

    void request();

}
