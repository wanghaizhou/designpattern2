package com.whz.test.proxy.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class RealSubject implements Subject {

    public void request() {
        System.out.println("真实请求");
    }

}
