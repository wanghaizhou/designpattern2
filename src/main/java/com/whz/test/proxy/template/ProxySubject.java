package com.whz.test.proxy.template;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ProxySubject implements Subject {

    private Subject subject;

    public ProxySubject(Subject subject) {
        this.subject = subject;
    }

    public void request() {
        System.out.println("代理请求");
        subject.request();
    }
}
