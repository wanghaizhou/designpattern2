package com.whz.test.proxy;

import com.whz.test.proxy.story.GiveGift;
import com.whz.test.proxy.story.ProxyGiveGift;
import com.whz.test.proxy.story.Pursuit;
import com.whz.test.proxy.story.SchoolGirl;
import com.whz.test.proxy.template.ProxySubject;
import com.whz.test.proxy.template.RealSubject;
import com.whz.test.proxy.template.Subject;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Test {

    public static void main(String[] args) {
        Subject subject =new ProxySubject(new RealSubject());
        subject.request();

        SchoolGirl schoolGirl = new SchoolGirl("美女甲");

        Pursuit pursuit = new Pursuit("马小蛋");
        pursuit.setLover(schoolGirl);

        GiveGift giveGift = new ProxyGiveGift(pursuit);
        giveGift.giveDollers();
        giveGift.giveFlowers();
        giveGift.giveChocolate();
    }

}
