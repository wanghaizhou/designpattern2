package com.whz.test.proxy.dynamicproxy;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * Created by wanghaizhou on 2018/4/8.
 */
public class CostTimePrinterProxy {

    /*Object object;

    public CostTimePrinterProxy(Object object) {
        this.object = object;
    }*/

    public static Object forProxy(Object object){
        return Proxy.newProxyInstance(object.getClass().getClassLoader(),object.getClass().getInterfaces(),new CostTimePrinterInvocationHandler(object));
    }

    static class CostTimePrinterInvocationHandler implements InvocationHandler{

        Object proxied;

        public CostTimePrinterInvocationHandler(Object proxied) {
            this.proxied = proxied;
        }

        @Override
        public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

            Method originalMethod = proxied.getClass().getMethod(method.getName(),method.getParameterTypes());
            //此处判断方法是否有注释，只能是originalMethod,不能是invoke方法里面的参数method,因为此处的method已经是运行时生成class的代理方法了
            if (!originalMethod.isAnnotationPresent(CostTimePrinter.class)){
                return method.invoke(proxied,args);
            }

            System.out.println("开始计时");
            Long time = System.currentTimeMillis();
            Object object = method.invoke(proxied,args);
            Long costtime = System.currentTimeMillis()-time;
            System.out.println(proxied.getClass().getName()+"."+method.getName()+"共计花费时长"+costtime);
            return object;
        }
    }

}
