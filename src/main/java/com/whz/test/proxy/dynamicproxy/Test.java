package com.whz.test.proxy.dynamicproxy;

/**
 * 动态代理
 * Created by wanghaizhou on 2018/4/8.
 */
public class Test {

    public static void main(String[] args) {
        Job job = new Job();
        JobAccess jobAccess = (JobAccess) CostTimePrinterProxy.forProxy(job);
        jobAccess.excute();
    }

}
