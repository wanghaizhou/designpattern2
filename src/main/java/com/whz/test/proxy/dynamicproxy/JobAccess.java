package com.whz.test.proxy.dynamicproxy;

/**
 * Created by wanghaizhou on 2018/4/8.
 */
public interface JobAccess {

    void excute();

}
