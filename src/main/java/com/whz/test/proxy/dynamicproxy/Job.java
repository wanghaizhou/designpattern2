package com.whz.test.proxy.dynamicproxy;

/**
 * Created by wanghaizhou on 2018/4/8.
 */
public class Job implements JobAccess{

    @CostTimePrinter
    public void excute(){
        System.out.println("任务执行中");
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("任务执行完毕");
    }

}
