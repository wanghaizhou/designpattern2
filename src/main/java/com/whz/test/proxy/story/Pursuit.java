package com.whz.test.proxy.story;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class Pursuit implements  GiveGift {

    private String name;
    private SchoolGirl lover;

    public Pursuit(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void giveFlowers() {
        if (lover!=null){
            System.out.println(name+" 送 "+lover.getName()+" 花");
        }
    }

    public void giveChocolate() {
        if (lover!=null){
            System.out.println(name+" 送 "+lover.getName()+" 巧克力");
        }
    }

    public void giveDollers() {
        if (lover!=null){
            System.out.println(name+" 送 "+lover.getName()+" 洋娃娃");
        }
    }

    public SchoolGirl getLover() {
        return lover;
    }

    public void setLover(SchoolGirl lover) {
        this.lover = lover;
    }
}
