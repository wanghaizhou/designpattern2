package com.whz.test.proxy.story;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public interface GiveGift {

    void giveFlowers();

    void giveChocolate();

    void giveDollers();

}
