package com.whz.test.proxy.story;

/**
 * Created by wanghaizhou on 2018/3/28.
 */
public class ProxyGiveGift implements GiveGift {

    private GiveGift giveGift;

    public ProxyGiveGift(GiveGift giveGift) {
        this.giveGift = giveGift;
    }

    public GiveGift getGiveGift() {
        return giveGift;
    }

    public void setGiveGift(GiveGift giveGift) {
        this.giveGift = giveGift;
    }

    public void giveFlowers() {
        giveGift.giveFlowers();
    }

    public void giveChocolate() {
        giveGift.giveChocolate();
    }

    public void giveDollers() {
        giveGift.giveDollers();
    }
}
