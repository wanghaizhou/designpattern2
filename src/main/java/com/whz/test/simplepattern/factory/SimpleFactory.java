package com.whz.test.simplepattern.factory;

/**
 * 简单工厂模式
 * Created by 海州 on 2018/4/1.
 */
public class SimpleFactory {

    Operate createOperate(String opertate){
        if ("1".equals(opertate)){
            return new Operate1();
        }else if ("2".equals(opertate)){
            return new Operate2();
        }
        return null;
    }

}
