package com.whz.test.simplepattern.factory;

/**
 * Created by 海州 on 2018/4/1.
 */
public interface Operate {

    void operate();

}
